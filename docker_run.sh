GPUS_NB=1 # set to 0 if you want to run on cpu
LOCAL_PATH="/home/giani/Documents/tmp/keras"
DATA_VOLUME="$LOCAL_PATH/datasets:/home/magician/app/data"
CHECKPOINT_VOLUME="$LOCAL_PATH/checkpoints:/home/magician/app/checkpoints"
MODEL_VOLUME="$LOCAL_PATH/models:/home/magician/app/models"

sudo docker run --gpus $GPUS_NB \
                -v $DATA_VOLUME \
                -v $CHECKPOINT_VOLUME \
                -v $MODEL_VOLUME \
                -ti keras:example \
                python mnist-classification.py -m train

sudo docker run --gpus $GPUS_NB \
                -v $DATA_VOLUME \
                -v $CHECKPOINT_VOLUME \
                -v $MODEL_VOLUME \
                -ti keras:example \
                python mnist-classification.py -m eval -p ./checkpoints