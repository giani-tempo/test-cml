import argparse
import numpy as np
import os
import random as rn
import tensorflow as tf

from tensorflow.keras.datasets import mnist
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.layers import Conv2D, Dense, Flatten, MaxPooling2D
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.utils import to_categorical

def load_local_data(path):
	with np.load(path) as data:
		trainX = data['x_train']
		trainY = data['y_train']
		testX = data['x_test']
		testY = data['y_test']
	return (trainX, trainY), (testX, testY)

# load train and test dataset
def load_dataset(data):
	# load local dataset
	if os.path.exists(data):
		mnist_data = os.path.join(data, 'mnist.npz')
		(trainX, trainY), (testX, testY) = load_local_data(mnist_data)
	# download dataset
	else:
		(trainX, trainY), (testX, testY) = mnist.load_data()
	# reshape dataset to have a single channel
	trainX = trainX.reshape((trainX.shape[0], 28, 28, 1))
	testX = testX.reshape((testX.shape[0], 28, 28, 1))
	# one hot encode target values
	trainY = to_categorical(trainY)
	testY = to_categorical(testY)
	return trainX, trainY, testX, testY

# scale pixels
def prep_pixels(train, test):
	# convert from integers to floats
	train_norm = train.astype('float32')
	test_norm = test.astype('float32')
	# normalize to range 0-1
	train_norm = train_norm / 255.0
	test_norm = test_norm / 255.0
	# return normalized images
	return train_norm, test_norm

# define cnn model
def define_model():
	model = Sequential()
	model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', input_shape=(28, 28, 1)))
	model.add(MaxPooling2D((2, 2)))
	model.add(Flatten())
	model.add(Dense(100, activation='relu', kernel_initializer='he_uniform'))
	model.add(Dense(10, activation='softmax'))
	# compile model
	opt = SGD(learning_rate=0.01, momentum=0.9)
	model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
	return model

def train_model(data, checkpoint):
    # load dataset
    trainX, trainY, testX, testY = load_dataset(data)
    # prepare pixel data
    trainX, testX = prep_pixels(trainX, testX)
    # define model
    model = define_model()
    # create callbacks 
    callbacks = [ModelCheckpoint(checkpoint)]
    # fit model
    history = model.fit(trainX, trainY, epochs=5, batch_size=32, 
        validation_data=(testX, testY), verbose=1, callbacks=callbacks)
    # evaluate model
    model = load_model(checkpoint)
    _, acc = model.evaluate(testX, testY, verbose=0)
    print('YOUR SCORE IS: %.3f' % (acc * 100.0))

def eval_model(data, pretrained):
    assert data and os.path.exists(data), 'Provide model data for evaluation'
    assert pretrained and os.path.exists(pretrained), 'Provide model path for evaluation'
    # load dataset
    trainX, trainY, testX, testY = load_dataset(data)
    # prepare pixel data
    trainX, testX = prep_pixels(trainX, testX)
    # load model
    model = load_model(pretrained)
	# evaluate model
    _, acc = model.evaluate(testX, testY, verbose=0)
    print('YOUR SCORE IS: %.3f' % (acc * 100.0))

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("--mode", "-m", help="Mods: train, eval")
	parser.add_argument("--data", "-d", default='./data', help="Path to train/test data")
	parser.add_argument("--pretrained", "-p", default='./models', help="Path from where to load model")
	parser.add_argument("--checkpoint", "-c", default='./checkpoints', help="Path where to save model")
	args = parser.parse_args()

	SEED = 42
	
	os.environ['PYTHONHASHSEED'] = str(SEED)
	np.random.seed(SEED)
	rn.seed(SEED)
	tf.random.set_seed(SEED)

	if args.mode == 'train':
		train_model(args.data, args.checkpoint)
	elif args.mode == 'eval':
		eval_model(args.data, args.pretrained)