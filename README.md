# Setting up development environment

## Dependencies

You will need to install these in order to create new docker images locally.

### Nvidia Docker

```The NVIDIA Container Toolkit allows users to build and run GPU accelerated Docker containers.```

**Useful links:**
-- [Instalation Guide](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#installing-on-ubuntu-and-debian)
-- [GitHub link](https://github.com/NVIDIA/nvidia-docker)

:warning: **Windows/macOS devices**: Are NOT officially supported!

<br/>

## Creating the docker image

### Creating linear regression image

```console
sudo docker build -f Dockerfile_keras -t keras:example .
```

### Check that nvidia drivers are available

```console
sudo docker run --gpus 1 -ti keras:example nvidia-smi
```

### Training the model using CPU

```console
sudo docker run -ti keras:example python3 mnist-classification.py -m train
```

### Training the model using GPU

```console
sudo docker run --gpus 1 -ti keras:example python3 mnist-classification.py
```

:question: **In case you dont know what the arguments mean**: You can use **docker *your_command* --help**

<br/>

## Resources

### Nvidia CUDA+cudnn parent images to start from

```CUDA is a parallel computing platform and programming model developed by NVIDIA for general computing on graphical processing units (GPUs).```

**Useful links:**
-- [Tensorflow DockerHub link](https://hub.docker.com/r/tensorflow/tensorflow)
-- [Nvidia DockerHub link](https://hub.docker.com/r/nvidia/cuda)
